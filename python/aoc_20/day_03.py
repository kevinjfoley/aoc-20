from functools import reduce
from operator import mul
import math


def a(tree_map):

    map_width = len(tree_map[0])
    x_pos = 0
    path = []

    for row in tree_map[1:]:
        x_pos += 3

        pos = (row * math.ceil(x_pos + 1 / map_width))[x_pos]

        path.append(pos)

    return len([i for i in path if i == "#"])


def b(tree_map):

    map_width = len(tree_map[0])
    paths = []

    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    for delta_x, delta_y in slopes:
        x_pos = 0
        path = []
        for row in tree_map[delta_y::delta_y]:
            x_pos += delta_x

            pos = (row * math.ceil(x_pos + 1 / map_width))[x_pos]

            path.append(pos)

        paths.append(path)

    return reduce(mul, [len([i for i in path if i == "#"]) for path in paths], 1)


if __name__ == "__main__":
    with open("./inputs/day_03.txt") as f:
        day_3_input = f.read().strip().split("\n")

    day_3_input = [list(i) for i in day_3_input]

    print(a(day_3_input))
    print(b(day_3_input))
