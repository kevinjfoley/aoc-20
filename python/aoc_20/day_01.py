import itertools


def a(expenses: list) -> int:

    expense_combos = itertools.combinations(expenses, 2)

    expenses_2020 = [e for e in expense_combos if sum(e) == 2020]

    return expenses_2020[0][0] * expenses_2020[0][1]


def b(expenses: list) -> int:

    expense_combos = itertools.combinations(expenses, 3)

    expenses_2020 = [e for e in expense_combos if sum(e) == 2020]

    return expenses_2020[0][0] * expenses_2020[0][1] * expenses_2020[0][2]


if __name__ == "__main__":
    with open("./inputs/day_01.txt") as f:
        day_1_input = f.read().strip().split("\n")

    day_1_input = [int(i) for i in day_1_input]

    print(a(day_1_input))
    print(b(day_1_input))
