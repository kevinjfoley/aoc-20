import math
from functools import reduce


def parse_bp(bp):
    seat_range = (0, 127, 0, 7)

    lf = {
        "F": lambda r: (r[0], r[1] - math.ceil((r[1] - r[0]) / 2), r[2], r[3]),
        "B": lambda r: (r[0] + math.ceil((r[1] - r[0]) / 2), r[1], r[2], r[3]),
        "L": lambda r: (r[0], r[1], r[2], r[3] - math.ceil((r[3] - r[2]) / 2)),
        "R": lambda r: (r[0], r[1], r[2] + math.ceil((r[3] - r[2]) / 2), r[3]),
    }

    seat_range = reduce(lambda r, f: f(r), [lf[l] for l in bp], seat_range)

    return (seat_range[0], seat_range[2])


def a(boarding_passes):

    seat_ids = []
    for bp in boarding_passes:
        r, c = parse_bp(bp)
        seat_ids.append(r * 8 + c)

    return max(seat_ids)


def b(boarding_passes):

    seat_ids = []
    for bp in boarding_passes:
        r, c = parse_bp(bp)
        seat_ids.append(r * 8 + c)

    for seat in range(min(seat_ids), max(seat_ids)):
        if [s in seat_ids for s in [seat - 1, seat, seat + 1]] == [1, 0, 1]:
            return seat


if __name__ == "__main__":
    with open("./inputs/day_05.txt") as f:
        day_5_input = f.read().strip().split("\n")

    print(a(day_5_input))
    print(b(day_5_input))
