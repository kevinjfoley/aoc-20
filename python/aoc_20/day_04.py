import re


def a(passports: list) -> int:

    expected_fields = {
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
        # "cid",
    }

    valid_passports = 0
    passport_fields = []

    for line in passports + [""]:
        if line.strip() == "":
            if len(set(passport_fields) & expected_fields) == len(expected_fields):
                valid_passports += 1
            passport_fields = []
        else:
            for field_value in line.split(" "):
                field, value = field_value.split(":")
                passport_fields.append(field)

    return valid_passports


def b(passports: list) -> int:
    def validate_hgt(hgt):
        height, unit = hgt[:-2], hgt[-2:]
        if height.isdigit():
            if unit == "in":
                return 59 <= int(height) <= 76
            elif unit == "cm":
                return 150 <= int(height) <= 193

    expected_fields = {
        "byr": lambda x: x.isdigit() and (1920 <= int(x) <= 2002),
        "iyr": lambda x: x.isdigit() and (2010 <= int(x) <= 2020),
        "eyr": lambda x: x.isdigit() and (2020 <= int(x) <= 2030),
        "hgt": validate_hgt,
        "hcl": lambda x: re.match("^#[a-f0-9]{6}$", x),
        "ecl": lambda x: x in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
        "pid": lambda x: re.match("^\d{9}$", x),
        # "cid",
    }

    valid_passports = 0
    passport_fields = []

    for line in passports + [""]:
        if line.strip() == "":
            if len(set(passport_fields) & set(expected_fields.keys())) == len(
                expected_fields
            ):
                valid_passports += 1
            passport_fields = []
        else:
            for field_value in line.split(" "):
                field, value = field_value.split(":")
                if field != "cid" and expected_fields[field](value):
                    passport_fields.append(field)

    return valid_passports


if __name__ == "__main__":
    with open("./inputs/day_04.txt") as f:
        day_4_input = f.read().strip().split("\n")

    print(a(day_4_input))
    print(b(day_4_input))
