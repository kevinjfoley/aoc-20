def a(passwords: list) -> int:

    valid_pws = []

    for policy, pw in [combos.split(":") for combos in passwords]:
        req, letter = policy.split(" ")
        req_min, req_max = [int(r) for r in req.split("-")]

        letters = [l for l in list(pw) if l == letter]

        if req_min <= len(letters) <= req_max:
            valid_pws.append(pw)

    return len(valid_pws)


def b(passwords: list) -> int:

    valid_pws = []

    for policy, pw in [combos.split(":") for combos in passwords]:
        pw = pw.strip()
        positions, letter = policy.split(" ")
        pos_1, pos_2 = [int(r) - 1 for r in positions.split("-")]

        letters = (list(pw)[pos_1], list(pw)[pos_2])

        if sum([l == letter for l in letters]) == 1:
            valid_pws.append(pw)

    return len(valid_pws)


if __name__ == "__main__":
    with open("./inputs/day_02.txt") as f:
        day_2_input = f.read().strip().split("\n")

    print(a(day_2_input))
    print(b(day_2_input))
