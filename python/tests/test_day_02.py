from aoc_20.day_02 import a, b


def test_a():
    a(["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc",]) == 2


def test_b():
    b(["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc",]) == 1
