from aoc_20.day_1 import a, b


def test_a():
    assert a([1721, 979, 366, 299, 675, 1456,]) == 514579


def test_b():
    assert b([1721, 979, 366, 299, 675, 1456,]) == 241861950
