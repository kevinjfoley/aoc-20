(ns advent-of-code.day-11
  (:require [clojure.string :as s]
            [clojure.java.io :refer [resource]]))

(defn parse-input
  ""
  [input]
  (map vec (s/split-lines input)))

(defn get-adj-seats
  ""
  [seat-map col-n i]
  (let [map-size (count seat-map)
        row-n (/ map-size col-n)
        i-row (int (/ i col-n))
        i-col (mod i col-n)
        n-rows-above (dec i-row)
        n-rows-below (- row-n i-row)
        n-cols-left (inc i-col)
        n-cols-right (- col-n i-col)
        north (map #(- i (* col-n %)) (range 1 (+ 2 n-rows-above)))
        south (map #(+ i (* col-n %)) (range 1 n-rows-below))
        east (map #(+ i %) (range 1 n-cols-right))
        west (map #(- i %) (range 1 n-cols-left))
        north-west (map #(- (first %) (second %)) (map vector north (range 1 n-cols-left)))
        north-east (map #(+ (first %) (second %)) (map vector north (range 1 n-cols-right)))
        south-west (map #(- (first %) (second %)) (map vector south (range 1 n-cols-left)))
        south-east (map #(+ (first %) (second %)) (map vector south (range 1 n-cols-right)))
        ]
    {:n north
     :s south
     :e east
     :w west
     :nw north-west
     :ne north-east
     :sw south-west
     :se south-east}))

(defn get-seat-value
  ""
  [f adj-seats-i seat seat-map tol]
  (let [adj-seat-values (map (fn [direction-indicies]
                               (some #(and (f (nth seat-map %))
                                           (nth seat-map %))
                                     direction-indicies))
                             (vals adj-seats-i))]
    (if (= seat \.)
      \.
      (as-> adj-seat-values $
        (frequencies $)
        (case seat
          \L (if (= 0 (get $ \# 0)) \# \L)
          \# (if (<= tol (get $ \# 0)) \L \#))))))


(defn part-1
  ([input]
   (part-1 nil
           (vec (apply concat (parse-input input)))
           (count (first (parse-input input))) 0))
  ([_ flat-seat-map n-col i]
   (as-> (range (count flat-seat-map)) $
     (map #(vector (nth flat-seat-map %) (get-adj-seats flat-seat-map n-col %)) $)
     (map #(get-seat-value (complement nil?) (second %) (first %) flat-seat-map 4) $)

     (if (or (= i 5) (= flat-seat-map $))
       (get (frequencies $) \#)
       (recur nil (vec $) n-col (inc 1))))))

(defn part-2
  ([input]
   (part-2 nil
           (vec (apply concat (parse-input input)))
           (count (first (parse-input input))) 0))
  ([_ flat-seat-map n-col i]
   (as-> (range (count flat-seat-map)) $
     (map #(vector (nth flat-seat-map %) (get-adj-seats flat-seat-map n-col %)) $)
     (map #(get-seat-value (fn [seat] (contains? #{\L \#} seat)) (second %) (first %) flat-seat-map 5) $)

     (if (or (= i 10) (= flat-seat-map $))
       (get (frequencies $) \#)
       (recur nil (vec $) n-col (inc 1))))))
