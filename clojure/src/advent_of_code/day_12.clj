(ns advent-of-code.day-12
  (:require [clojure.string :as s]
            [clojure.java.io :refer [resource]]))

(def ex-input (slurp (resource "day-12-example.txt")))
(def input (slurp (resource "day-12.txt")))

(defn parse-input
  [input]
  (->>
   (s/split-lines input)
   (map #(s/split % #"\B" 2))
   (map #(vector (first %) (Integer/parseInt (second %))))))

(parse-input ex-input)
;; => (["F" 10] ["N" 3] ["F" 7] ["R" 90] ["F" 11])

(defn rotate
  [cur-dir way amt]
  (let [dirs ["N" "E" "S" "W"]
        amt (/ (mod amt 360) 90)]
    (as-> (if (= way "L") (reverse dirs) dirs) $
      (cycle $)
      (drop-while #(not= cur-dir %) $)
      (nth $ amt))))

(map #(rotate "E" "R" %) '(0 90 180 270 360 450 540 630 720))
;; => ("E" "S" "W" "N" "E" "S" "W" "N" "E")

(defn get-direction
  "Return direction being faced after a step"
  [instruction cur-dir]
  (let [[action n] instruction]
    (case action
      ("R" "L")(rotate cur-dir action n)
      cur-dir)))

(defn part-1
  "Day 12 Part 1"
  [input]
  (let [parsed-input (parse-input input)
        mvmt
        (->>
         (reduce #(conj %1 (get-direction %2 (last %1))) ["E"] parsed-input)
         (map #(if (= "F" (first %1)) [%2 (second %1)] %1) parsed-input)
         (filter #(not= "R" (first %1)))
         (group-by first)
         (map (fn [[k vals]] [k (apply + (map #(second %) vals))]))
         (reduce #(assoc %1 (first %2) (second %2)) {}))]
    (+ (Math/abs (- (get mvmt "N" 0)
                    (get mvmt "S" 0)))
       (Math/abs (- (get mvmt "E" 0)
                    (get mvmt "W" 0))))))
(part-1 ex-input)
;; => 25
(part-1 input)
;; => 2057

(defn move-ship
  [waypoint cur-pos n]
  (map + cur-pos (map #(* n %) waypoint)))

(move-ship [1 10] [0 0] 10)
;; => (10 100)

(defn move-waypoint
  [waypoint dir n]
  (case dir
    "E" [(+ (first waypoint) n) (second waypoint)]
    "W" [(- (first waypoint) n) (second waypoint)]
    "N" [(first waypoint) (+ (second waypoint) n)]
    "S" [(first waypoint) (- (second waypoint) n)]))

(move-waypoint [10 1] "N" 3)
;; => [10 4]

(defn rotate-waypoint
  [waypoint dir n]
  (let [times (/ (mod n 360) 90)
        times (if (= "L" dir) (mod (- 4 times) 4) times)]
    (case times
      0 waypoint
      1 [(second waypoint) (- 0 (first waypoint))]
      2 (map #(- 0 %) waypoint)
      3 [(- 0 (second waypoint)) (first waypoint)])))

(rotate-waypoint '(10 4) "R" 90)
;; => [4 -10]

(defn move
  ""
  [{:keys [waypoint cur-pos]}
   [action n]]
  (case action
        "F" {:waypoint waypoint
             :cur-pos (move-ship waypoint cur-pos n)}
        ("R" "L") {:waypoint (rotate-waypoint waypoint action n)
                   :cur-pos cur-pos}
        {:waypoint (move-waypoint waypoint action n)
         :cur-pos cur-pos}))

(move {:waypoint [10 1] :cur-pos [0 0]} ["F" 10])
;; => {:waypoint [10 1], :cur-pos (100 10)}

(reduce #(move %1 %2) {:waypoint [10 1] :cur-pos [0 0]} (parse-input ex-input))
(defn part-2
  "Day 12 Part 2"
  [input]
  (let [parsed-input (parse-input input)]
    (->>
     (reduce #(move %1 %2) {:waypoint [10 1] :cur-pos [0 0]} parsed-input)
     :cur-pos
     (map #(Math/abs %))
     (apply +))))


(part-2 ex-input)
;; => 286
(part-2 input)
;; => 71504
