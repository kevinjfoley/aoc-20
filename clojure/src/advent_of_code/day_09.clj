(ns advent-of-code.day-09
  (:require [clojure.string :as s]
            clojure.set))

(defn parse-input
  ""
  [input]
  (map bigint (s/split-lines input)))

(defn part-1
  "Day 09 Part 1"
  [input]
  (let [parsed-input (parse-input input)]
    (some identity
          (map (fn [win]
                 (let [pair (vector (last win) (set (butlast win)))
                       valids (map (partial - (first pair)) (second pair))
                       inter (clojure.set/intersection (second pair) (set valids))]
                   (when (empty? inter)
                     (first pair))))
               (partition 26 1 parsed-input)))))

(defn part-2
  "Day 09 Part 2"
  [input]
  (let [parsed-input (parse-input input)
        p1-solution (part-1 input)
        match (first
               (some not-empty
                     (map (fn [seq-n]
                            (let [sums (map #(vector (apply + %) %)
                                            (partition seq-n 1 parsed-input))]
                              (filter #(= p1-solution (first %)) sums)))
                          (range 2 1000))))]
    (+ (apply min (second match)) (apply max (second match)))))
