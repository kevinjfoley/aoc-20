(ns advent-of-code.day-06)

(defn parse-input
  ""
  [input]
  (map clojure.string/split-lines (clojure.string/split input #"\n\n")))

(defn count-letter-freq
  "Counts use of each letter across responses"
  [responses]
  (frequencies (clojure.string/join responses)))

(defn get-group-size
  "Count number of users in group"
  [responses]
  (count responses))

(defn part-1
  "Day 06 Part 1"
  [input]
  (let [parsed-input (parse-input input)
        letter-freqs (map count-letter-freq parsed-input)]
    (reduce + (map count letter-freqs))))

(defn part-2
  "Day 06 Part 2"
  [input]
  (let [parsed-input (parse-input input)
        letter-freqs (map count-letter-freq parsed-input)
        group-size (map count parsed-input)]
    (reduce + (map count
                   (map (fn [[cnts size]]
                          (filter #(= size (second %))
                                  cnts))
                        (map vector letter-freqs group-size))))))
