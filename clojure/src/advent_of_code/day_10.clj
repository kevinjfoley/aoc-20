(ns advent-of-code.day-10
  (:require [clojure.java.io :refer [resource]]
            [clojure.string :as s]))

(defn parse-input
  [input]
  (map #(Integer. %) (s/split-lines input)))

(defn calc-jumps
  ""
  [adapters]
  (reduce #(conj %1
                 {:rating %2
                  :jump (- %2 (or (:rating (last %1))
                                  0))})
          []
          (sort adapters)))

(defn calc-opt
  [jump-map]
  (reverse
   (reduce
    #(conj %1 (assoc %2 :optional (and (not= (:jump %2) 3)
                                       (not= (:jump (last %1)) 3))))
    []
    (reverse jump-map))))

(defn ! [n] (reduce *' (range 1 (inc n))))
(defn combo
  [n r]
  (/ (! n) (* (! r) (! (- n r)))))

(defn opt-grp-combos
  [grp]
  (if (>= 3 (count grp))
    (reduce #(+ %1 (combo (count grp) %2)) 0
            (range (if (= (count grp) 3) 1 0)
                   (inc (count grp))))
    (reduce + 0 (map opt-grp-combos (partition 3 3 nil grp)))))


(defn part-1
  "Day 10 Part 1"
  [input]
  (let [adapters (parse-input input)
        jump-map (calc-jumps adapters)
        ;; Add device
        jump-map (conj jump-map {:rating (+ 3 (:rating (last jump-map)))
                                 :jump 3})
        jump-freq (frequencies (map :jump jump-map))]
    (* (get jump-freq 1) (get jump-freq 3))))

(defn part-2
  "Day 10 Part 2"
  [input]
  (let [adapters (parse-input input)
        jump-map (calc-jumps adapters)
        ;; Add device
        jump-map (conj jump-map {:rating (+ 3 (:rating (last jump-map)))
                                 :jump 3})
        jump-map-opt (calc-opt jump-map)
        part-jm (partition-by :optional jump-map-opt)]
    (reduce * (map (fn [grp]
                     (if (:optional (first grp))
                       (opt-grp-combos grp)
                       1))
                   part-jm))))
