(ns advent-of-code.day-07
  (:require [clojure.string :as s]))

(defn parse-input
  ""
  [input]
  (reduce  (fn [rule-map rule]
             (let
              [rule-parts (s/split rule #" bag[s., ]*(contain )?")]
               (assoc rule-map
                      (first rule-parts)
                      (map
                       (fn [content]
                         (if-not (= content "no other")
                           (let [[q c] (s/split content #" " 2)]
                             {:color c :quantity (Integer. q)})
                           {}))
                       (rest rule-parts)))))
           {}
           (s/split-lines input)))

(defn allowed-contents
  ""
  [bag-color rules]
  (let [bag-rules (get rules bag-color)]
    (if (empty? bag-rules)
      nil
      (remove nil?
              (concat (map :color bag-rules)
                      (set (flatten (map #(allowed-contents (:color %) rules) bag-rules))))))))

(defn number-of-contents
  ""
  [rules bag-color]
  (let [bag-rules (get rules bag-color)]
    (if (empty? bag-rules)
      0
      (reduce +
              (concat
               (map :quantity bag-rules)
               (map #(* %1 %2)
                    (map :quantity bag-rules)
                    (map #(number-of-contents rules %) (map :color bag-rules))))))))

(defn part-1
  "Day 07 Part 1"
  [input]
  (let [rules (parse-input input)]
    (count (filter
            identity
            (map #(some #{"shiny gold"} %)
                 (map #(allowed-contents % rules)
                      (keys rules)))))))

(defn part-2
  "Day 07 Part 2"
  [input]
  (let [rules (parse-input input)]
    (number-of-contents rules "shiny gold")))
