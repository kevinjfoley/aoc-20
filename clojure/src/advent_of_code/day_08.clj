(ns advent-of-code.day-08
  (:require [clojure.string :as s]))

(defn parse-input
  [input]
  (map (fn [[cmd arg]] [cmd (Integer. arg)])
       (map #(s/split % #" ") (s/split-lines input))))

(defn nop
  [acc arg]
  [acc 1])

(defn acc
  [acc arg]
  [(+ acc arg) 1])

(defn jmp
  [acc arg]
  [acc arg])

(defn exec
  [args]
  ;; (println (:cur args))
  (let [{:keys [acc cur cmds prev]} args
        [f arg] (nth cmds cur)
        [acc jump] ((resolve (symbol f)) acc arg)]
    {:acc acc
     :cur (+ cur jump)
     :cmds cmds
     :prev (conj prev cur)}))

(defn split-cmds [[proc unproc]]
  (let [
        ;; Reset last element of processed commands
        proc (if-not (empty? proc)
               (concat (butlast proc) [(replace {"jmp" "nop" "nop" "jmp"} (last proc))])
               proc)
        ;; Split with first item of b needing to be flipped
        [a b] (split-with (complement
                           #(contains? #{"nop" "jmp"} (first %))
                           ) unproc)]
    [(concat proc (concat a [(replace {"jmp" "nop" "nop" "jmp"} (first b))]))
     (rest b)]))

(defn part-1
  "Day 08 Part 1"
  [input]
  (let [parsed-input (parse-input input)]
    (last
     (take-while
      #(not (contains? (:prev %) (:cur %)))
      (iterate exec {:acc 0 :cur 0 :cmds parsed-input :prev #{}})))))

(defn part-2
  "Day 08 Part 2"
  [input]
  (let [parsed-input (parse-input input)]
    (exec
     (first
      (filter
       #(= (:cur %) (dec (count parsed-input)))
       (map
        (fn [cmds]
          ;; (print (s/join "\n" (apply concat cmds)))
          (last
           (take-while
            #(and (not (contains? (:prev %) (:cur %)))
                  (not (contains? (:prev %) (dec (count (:cmds %))))))
            (iterate exec {:acc 0 :cur 0 :cmds (apply concat cmds) :prev #{}}))))
        ;; All possible replacements of nop to jmp and jmp to nop
        (concat
         (take-while #(not (empty? (second %))) (iterate split-cmds [[] parsed-input]))
         ;; Not sure if there's a better way to include the element that `take-while` stops at
         (first (drop-while #(not (empty? (second %))) (iterate split-cmds [[] parsed-input]))))))))))
